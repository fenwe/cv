var langageId=null;
var IEL_contentId=null;
function load(){
    langageId = document.getElementById("langages");
    langageId.onmouseover = function () {
        /*langageId.style.textDecoration = "underline";*/
        langageId.style.borderBottom = "3px solid magenta";
        IEL_content.style.border = "3px solid magenta";
    }
    langageId.onmouseout = function () {
        langageId.style.borderBottom = "3px solid transparent";
        IEL_content.style.border = "3px solid transparent";
    }
}


function switchCpt(_eletId){
    var cptID=document.getElementById(_eletId);
    setStyleCompetences();
}

var cptIconeSources=[];
cptIconeSources['Technique']='img/cubes-joueur3.png';
cptIconeSources['Relationnel']='img/avec-logo-1.01.png';
cptIconeSources['Stress']='img/YinYang2.webp';

function setStyleCompetences(_cptNom){

    _cptTechniques=document.querySelectorAll(".cptIcone"+_cptNom);
    var _ctpNb=_cptTechniques.length;

    for (_i=0;_i<_ctpNb;_i++){

        if (_cptTechniques[_i].style.display=="none" || _cptTechniques[_i].style.display=="") {
            _cptTechniques[_i].style.display="inline";
            _cptTechniques[_i].src=cptIconeSources[_cptNom];
        }
        else{
            _cptTechniques[_i].style.display="none";
        }
    }

}


